#!/usr/bin/env bash
set -euo pipefail

rm -rf render/* || true
tk export render $1
kubeval render/*.yaml
