{
  _config+:: {
    vault: {
      postgresql: {
        username: 'vault',
        password: 'vault',
        database: 'vault',
        storage: '1Gi',
      },
    },
  },
  _images+:: {
    vault: {
      vault: 'vault:1.8.1',
      postgresql: 'postgres:13',
    },
  },
}
