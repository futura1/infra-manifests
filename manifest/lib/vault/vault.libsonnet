local k = import 'ksonnet-util/kausal.libsonnet';

local tanka = import 'tanka-util/main.libsonnet',
      helm = tanka.helm.new(std.thisFile);

local clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
      configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      deployment = k.apps.v1.deployment,
      persistentVolumeClaimTemplate = k.core.v1.persistentVolumeClaimTemplate,
      serviceAccount = k.core.v1.serviceAccount,
      statefulSet = k.apps.v1.statefulSet,
      subject = k.rbac.v1.subject,
      volumeMount = k.core.v1.volumeMount;

(import './config.libsonnet') + {
  local module = self,
  local c = $._config.vault,

  vault_config:: std.manifestJson({
    listener: {
      tcp: {
        address: '0.0.0.0:8200',
        tls_disable: 1,
      },
    },
    storage: {
      postgresql: {
        connection_url: 'postgres://%s:%s@%s:5432/%s?sslmode=disable' % [
          c.postgresql.username,
          c.postgresql.password,
          module.postgresql_service.metadata.name,
          c.postgresql.database,
        ],
      },
    },
  }),

  vault_deployment: deployment.new(
    'vault',
    replicas=1,
    containers=[
      container.new('vault', $._images.vault.vault) +
      container.withPorts([containerPort.new('http', 8200)]) +
      container.withArgs(['server']) +
      container.withEnvMap({
        VAULT_LOCAL_CONFIG: module.vault_config,
      }) +
      container.securityContext.capabilities.withAdd('IPC_LOCK'),
    ]
  ),

  vault_service: k.util.serviceFor(self.vault_deployment),

  postgresql_init: configMap.new('vault-postgresql-init', {
    'vault-migration.sql': importstr './vault-migration.sql',
  }),

  postgresql_sts:
    statefulSet.new(
      'vault-postgresql',
      replicas=1,
      containers=[
        container.new('postgresql', $._images.vault.postgresql) +
        container.withPorts([containerPort.new('postgresql', 5432)]) +
        container.withEnvMap({
          POSTGRES_USER: c.postgresql.username,
          POSTGRES_PASSWORD: c.postgresql.password,
          POSTGRES_DATABASE: c.postgresql.database,
        }) +
        container.withVolumeMounts([
          volumeMount.new('data', '/var/lib/postgresql/data', false),
        ]),
      ],
      volumeClaims=[
        persistentVolumeClaimTemplate.metadata.withName('data') +
        persistentVolumeClaimTemplate.spec.withAccessModes(['ReadWriteOnce']) +
        persistentVolumeClaimTemplate.spec.resources.withRequests({ storage: c.postgresql.storage }),
      ],
    ) +
    statefulSet.spec.withServiceName(self.postgresql_sts.metadata.name) +
    k.util.configMapVolumeMount(self.postgresql_init, '/docker-entrypoint-initdb.d'),

  postgresql_service: k.util.serviceFor(self.postgresql_sts),

  vswh: helm.template('vswh', './charts/vault-secrets-webhook', {
    namespace: 'vault',
  }),

  sa: serviceAccount.new('vault-tokenreview'),

  clusterRoleBinding:
    clusterRoleBinding.new('vault-tokenreview-binding') +
    clusterRoleBinding.withSubjects([
      subject.fromServiceAccount(self.sa) +
      subject.withNamespace('vault'),
    ]) +
    clusterRoleBinding.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    clusterRoleBinding.roleRef.withKind('ClusterRole') +
    clusterRoleBinding.roleRef.withName('system:auth-delegator'),
}
