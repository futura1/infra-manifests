local k = import 'ksonnet-util/kausal.libsonnet';

local container = k.core.v1.container,
      deployment = k.apps.v1.deployment,
      httpIngressPath = k.networking.v1.httpIngressPath,
      ingress = k.networking.v1.ingress,
      ingressRule = k.networking.v1.ingressRule,
      persistentVolumeClaimTemplate = k.core.v1.persistentVolumeClaimTemplate,
      port = k.core.v1.containerPort,
      service = k.core.v1.service,
      serviceAccount = k.core.v1.serviceAccount,
      statefulSet = k.apps.v1.statefulSet,
      volumeMount = k.core.v1.volumeMount;

(import './config.libsonnet') + {
  simple_app: {
    local module = self,
    local c = $._config.simpleapp,


    deployment:
      deployment.new(
        $._config.simpleapp.service.name + '-deployment',
        replicas=1,
        containers=[
          container.new($._config.simpleapp.service.name, $._images.simpleapp.service) +
          container.withPorts([port.new('http', $._config.simpleapp.service.port)]) +
          container.withEnvMap({
            username: c.postgresql.username,
            password: 'vault:' + c.postgresql.password_key,
            database: c.postgresql.database,
            host: 'simple-app-postgres',
            port: '5432',
          }),
        ]
      ) +
      deployment.spec.template.spec.withServiceAccountName(module.sa.metadata.name) +
      deployment.spec.template.metadata.withAnnotations({
        'vault.security.banzaicloud.io/vault-addr': $._config.simpleapp.vault.addr,
        'vault.security.banzaicloud.io/vault-role': $._config.simpleapp.vault.role,
      }),

    service: k.util.serviceFor(self.deployment),

    ingress:
      ingress.new($._config.simpleapp.service.name + '-ingress') +
      ingress.spec.withRules(
        ingressRule.withHost($._config.simpleapp.service.ingress_host) +
        ingressRule.http.withPaths(
          httpIngressPath.withPath('/') +
          httpIngressPath.withPathType('Prefix') +
          httpIngressPath.backend.service.withName(module.service.metadata.name) +
          httpIngressPath.backend.service.port.withNumber($._config.simpleapp.service.port)
        )
      ),

    database_sts:
      statefulSet.new(
        $._config.simpleapp.service.name + '-postgres',
        replicas=1,
        containers=[
          container.new('postgres', $._images.simpleapp.postgres) +
          container.withPorts([port.new('postgresql', 5432)]) +
          container.withEnvMap({
            POSTGRES_PASSWORD: 'vault:' + c.postgresql.password_key,
            POSTGRES_USER: c.postgresql.username,
            POSTGRES_DATABASE: c.postgresql.database,
          }) +
          container.withVolumeMounts([
            volumeMount.new('data', '/var/lib/postgresql/data', false),
          ]),
        ],

        volumeClaims=[
          persistentVolumeClaimTemplate.metadata.withName('data') +
          persistentVolumeClaimTemplate.spec.withAccessModes(['ReadWriteOnce']) +
          persistentVolumeClaimTemplate.spec.resources.withRequests({ storage: c.postgresql.storage }),
        ],
      ) +
      statefulSet.spec.withServiceName($._config.simpleapp.service.name + '-postgres') +
      statefulSet.spec.template.spec.withServiceAccountName(module.sa.metadata.name) +
      statefulSet.spec.template.metadata.withAnnotations({
        'vault.security.banzaicloud.io/vault-addr': $._config.simpleapp.vault.addr,
        'vault.security.banzaicloud.io/vault-role': $._config.simpleapp.vault.role,
      }),

    database_svc: k.util.serviceFor(self.database_sts),

    sa: serviceAccount.new($._config.simpleapp.service.name),
  },
}
