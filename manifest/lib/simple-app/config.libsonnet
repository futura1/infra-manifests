{
  _config+:: {
    simpleapp: {
      vault: {
        addr: 'http://vault.vault:8500',
        role: 'env',
      },
      service: {
        name: 'simple-app',
        port: 3000,
        ingress_host: 'example.com',
      },
      postgresql: {
        username: 'simpleapp',
        password_key: 'simpleapp',
        database: 'simpleapp',
        storage: '1Gi',
      },
    },
  },
  _images+:: {
    simpleapp: {
      service: 'registry.gitlab.com/futura1/simple-app:latest',
      postgres: 'postgres:13',
    },
  },
}
