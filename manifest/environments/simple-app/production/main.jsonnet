local images = import 'images.json';

local simple_app = import 'simple-app/simple-app.libsonnet';

simple_app {
  _config+:: {
    simpleapp+: {
      vault+: {
        addr: 'http://vault.vault:8200',
        role: 'production-simpleapp',
      },
      service+: {
        ingress_host: 'prod.simpleapp.anakaiti.net',
      },
      postgresql+: {
        password_key: 'kv/data/production/simple-app#password#1',
      },
    },
  },
  _images+:: images,
}
