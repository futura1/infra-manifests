local images = import 'images.json';

local simple_app = import 'simple-app/simple-app.libsonnet';

simple_app {
  _config+:: {
    simpleapp+: {
      vault+: {
        addr: 'http://vault.vault:8200',
        role: 'development-simpleapp',
      },
      service+: {
        ingress_host: 'dev.simpleapp.anakaiti.net',
      },
      postgresql+: {
        password_key: 'kv/data/development/simple-app#password#1',
      },
    },
  },
  _images+:: images,
}
