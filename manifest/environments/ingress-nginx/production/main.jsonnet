local tanka = import 'tanka-util/main.libsonnet',
      helm = tanka.helm.new(std.thisFile);

helm.template('ingress-nginx', './charts/ingress-nginx', {
  namespace: 'ingress-nginx',
  values: { controller: { hostNetwork: true } },
})
