# infra-manifests

Kubernetes manifests

```
bootstrap/  Non-tanka managed manifests
manifest/   Tanka manifests
```

## Further improvements

Currently, tanka envs are manually listed in .gitlab-ci.yml. We could use dynamic pipelines to auto-discover pipelines in the manifest.
However, gitlab child pipelines doesn't detect file changes & doesn't get kubeconfig from env. It's possible to fix this by manually adding kubeconfig to secrets & adding a custom file change detection setup.
